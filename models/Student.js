const mongoose = require('mongoose')
 
const Student = mongoose.Schema ({
    nama: {
        type: String,
        required: true
    },
    nim: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('student', Student)