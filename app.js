const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const route = require('./routes/index.js')
const PORT = process.env.PORT || 3000
const url = 'mongodb://localhost:27017/collect_test'
const app = express()

//middleware
app.use(express.json())
app.use(express.urlencoded({extended : true}))
app.use('/rmu', route)

//routes


//connect ke database

mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => {
    console.log(`Database connected!`)
}).catch((err) => {
    console.log(`Cannot connect to database!`, err);
    process.exit()
});

const db = mongoose.connection

//listen
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`)
})

