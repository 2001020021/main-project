const express = require('express')
const { studentRegistered } = require('../controller/rmu.controller.js')
const router = express.Router()

//router.get('/', getStudent)

router.post('/', studentRegistered)

module.exports = router